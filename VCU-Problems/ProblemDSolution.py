length = 0
minLeft = []
maxRight = []
numList = []
i = 0
count = 0

length = int(input())
while i < length:
    num = int(input())
    numList.append(num)
    i += 1

def findMaxMin(numbers):
    for indx in range(0, length):
        numMin = 0
        numMax = 0

        for i in numbers:
            if i < numbers[indx]:
                numMin += 1
            minLeft.append(numMin)

        for n in numbers:
            if n > numbers[indx]:
                numMax += 1
            maxRight.append(numMax)
    return maxRight, minLeft

def getSolution(count):
    count = 0
    for i in range(0, minLeft):
        count += (minLeft[i] * maxRight[i])
    
    return count

findMaxMin(numList)
getSolution(count)
print(count)