#include <stdio.h>
#include "minunit.h"
#include "average.c"

int tests_run = 0;

int a[] = {1, 2, 3, 4, 5};
int b[] = {5, 7};

static char * test_average_01() {
    mu_assert("error, average([1, 2, 3, 4, 5]) != 3", average(a, 5) == 3);
    return 0;
}

static char * test_average_02() {
    mu_assert("error, average([5, 7]) != 6", average(b, 2) == 6);
    return 0;
}

static char * all_tests() {
    mu_run_test(test_average_01);
    mu_run_test(test_average_02);
    return 0;
}

int main(int argc, char**argv) {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    }
    else{
        printf("ALL TESTS PASSED\n");
    }
    printf("Tests run: %d\n", tests_run);

    return result != 0;
}