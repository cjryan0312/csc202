int second(int a[], int size) {
    int i, large, second_large = 0;
    large = a[0];
    for (i = 1; i < size; i++) {
        if (a[i] > large) {
            large = a[i];
        }
    }
    second_large = a[0];

    for (i = 0; i < size; i++) {
        if (a[i] != large) {
            if (a[i] > second_large) {
                second_large = a[i];
            }
        }
    }
    printf("second largest is : %d \n", second_large);
    return second_large;
}