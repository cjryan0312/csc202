#include <stdio.h>
#include <math.h>
#include "minunit.h"
#include "makeNum.c"

int tests_run = 0;

int a[] = {2, 3, 0, 9};
int b[] = {1, 5, 7};

static char * test_makeNum_01() {
    mu_assert("error, {2, 3, 0, 9} != 2309", makeNum(a, 4) == 2309);
    return 0;
}
static char * test_makeNum_02() {
    mu_assert("error, {1, 5, 7} != 157", makeNum(b, 3) == 157);
    return 0;
}

static char * all_tests() {
    mu_run_test(test_makeNum_01);
    mu_run_test(test_makeNum_02);
    return 0;
}


int main(int argc, char**argv) {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    }
    else{
        printf("ALL TESTS PASSED\n");
    }
    printf("Tests run: %d\n", tests_run);

    return result != 0;
}