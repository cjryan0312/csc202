#include <stdio.h>
#include "minunit.h"
#include "second.c"

int tests_run = 0;

int a[] = {1, 2, 3, 4, 5};
int b[] = {1, 3, 2};

static char * test_second_01() {
    mu_assert("error, {1, 2, 3, 4, 5} != 4", second(a, 5) == 4);
    return 0;
}

static char * test_second_02() {
    mu_assert("error, {3, 2, 1} != 2", second(b, 3) == 2);
    return 0;
}

static char * all_tests() {
    mu_run_test(test_second_01);
    mu_run_test(test_second_02);
    return 0;
}

int main(int argc, char**argv) {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    }
    else{
        printf("ALL TESTS PASSED\n");
    }
    printf("Tests run: %d\n", tests_run);

    return result != 0;
}