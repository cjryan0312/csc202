int smallpos(int a[], int size) {
    int i, pos, small;
    small = a[0];
    pos = 0;

    for (i = 0; i < size; i++) {
        if (a[i] < small) {
            small = a[i];
            pos = i;
        }
    }
    return pos;
}