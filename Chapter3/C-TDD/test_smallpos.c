#include <stdio.h>
#include "minunit.h"
#include "smallpos.c"

int tests_run = 0;

int a[] = {7, 6, 5, 14, 3};
int b[] = {1, 5, 7};

static char * test_smallpos_01() {
    mu_assert("error, {7, 6, 5, 14, 3} != 4", smallpos(a, 5) == 4);
    return 0;
}

static char * test_smallpos_02() {
    mu_assert("error, {1, 5, 7} != 0", smallpos(b, 3) == 0);
    return 0;
}

static char * all_tests() {
    mu_run_test(test_smallpos_01);
    mu_run_test(test_smallpos_02);
    return 0;
}

int main(int argc, char**argv) {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    }
    else{
        printf("ALL TESTS PASSED\n");
    }
    printf("Tests run: %d\n", tests_run);

    return result != 0;
}