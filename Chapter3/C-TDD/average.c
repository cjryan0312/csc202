int average(int a[], int size) {
    int i, sum = 0;
    float mean = 0.0;
    for (i = 0; i < size; i++) {
        sum += a[i];
    }
    mean = (float)sum/size;
    return mean;
}