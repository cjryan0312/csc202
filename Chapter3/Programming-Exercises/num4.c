int sumSQ(int a[], int size) {
    int i, sum = 0, adder = 0;
    for (i = 0; i < size; i++) {
       adder = a[i];
       adder = adder * adder;
       sum += adder;
    }
    return sum;
}