#include <stdio.h>
#include "../C-TDD/minunit.h"
#include "num4.c"

int tests_run = 0;

int a[] = {2, 2};
int b[] = {2,2,2};

static char * test_num4_01() {
    mu_assert("error, sumSQ(2, 2) != 8", sumSQ(a, 2) == 8);
    return 0;
}

static char * test_num4_02() {
    mu_assert("error, sumSQ(2, 2) != 8", sumSQ(b, 3) == 12);
    return 0;
}

static char * all_tests() {
    mu_run_test(test_num4_01);
    mu_run_test(test_num4_02);
    return 0;
}

int main(int argc, char**argv) {
    char *result = all_tests();
    if (result != 0) {
        printf("%s\n", result);
    }
    else{
        printf("ALL TESTS PASSED\n");
    }
    printf("Tests run: %d\n", tests_run);

    return result != 0;
}
