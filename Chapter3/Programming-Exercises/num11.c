#include <stdio.h>

int main() {
    int i, j;
    int sizeOdd = 0, sizeEven = 0;
    int num = 0;
    int arr_1[3][3] = {{2, 1, 3}, {3, 2, 1}, {1,2,3}};
    int arr_2[3][3] = {{3, 1, 2}, {1, 2, 3}, {3,2,1}};

    int arrOdd[100];
    int arrEven[100];    
    
    
    for (i = 0; i < 3; i++) {
       for (j = 0; j < 3; j++) {
           num = arr_1[i][j];
           if (num % 2 == 1) {
               arrOdd[sizeOdd] = num;
               sizeOdd++;
           }
           else {
               arrEven[sizeEven] = num;
               sizeEven++;
           }
       }   
    }

    for (i = 0; i < 3; i++) {
       for (j = 0; j < 3; j++) {
           num = arr_2[i][j];
           if (num % 2 == 1) {
               arrOdd[sizeOdd] = num;
               sizeOdd++;
           }
           else {
               arrEven[sizeEven] = num;
               sizeEven++;
           }
       }   
    }
    
   
   
    for (i = 0; i < sizeOdd; i++)
    {
       printf("\t %d", arrOdd[i]);
    }
   
    printf("\n");
    for (i = 0; i < sizeEven; i++)
    {
       printf("\t %d", arrEven[i]);
    }

    return 0;
}