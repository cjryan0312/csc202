#include <stdio.h>
#include <string.h>

typedef struct 
{
    char first_name[20];
    char mid_name[20];
    char last_name[20];
}NAME;

typedef struct
{
    int day;
    int month;
    int year;
}DATE;

typedef struct
{
    int eng_grd;
    int math_grd;
    int CompSci_grd;
}GRADES;


typedef struct
{
    int roll_no;
    NAME name;
    char sex[2]; // MUST BE 2
    DATE DOB;
    GRADES grades;
}student;

int main() {
    int i;

    student stud[5];
    stud[0].roll_no = 001;
    strcpy(stud[0].name.first_name, "Bart");
    strcpy(stud[0].name.mid_name, "Tarb");
    strcpy(stud[0].name.last_name, "Trab");
    strcpy(stud[0].sex, "M");
    stud[0].DOB.day = 16;
    stud[0].DOB.month = 12;
    stud[0].DOB.year = 2003;
    stud[0].grades.eng_grd = 95;
    stud[0].grades.math_grd = 14;
    stud[0].grades.CompSci_grd = 59;

    stud[1].roll_no = 002;
    strcpy(stud[1].name.first_name, "Buffalo");
    strcpy(stud[1].name.mid_name, "buffalo");
    strcpy(stud[1].name.last_name, "Buffalo");
    strcpy(stud[1].sex, "M");
    stud[1].DOB.day = 30;
    stud[1].DOB.month = 4;
    stud[1].DOB.year = 2004;
    stud[1].grades.eng_grd = 75;
    stud[1].grades.math_grd = 75;
    stud[1].grades.CompSci_grd = 75;

    printf("\n FINDING STUDENT BORN ON 12/16/2003 \n");    
    for (i = 0; i < 3; i++)
    {
        if ( (stud[i].DOB.month == 12) && (stud[i].DOB.day == 16) && (stud[i].DOB.year == 2003) )
        {
            printf("Student %s %s %s was born on 12/16/2003", stud[i].name.first_name, stud[i].name.mid_name, stud[i].name.last_name);
        }   
    }
     
    return 0;
}
/*
    
*/
/*
    
*/