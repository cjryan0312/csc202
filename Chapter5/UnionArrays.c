#include <stdio.h>
union POINT
{
    int x, y;
};

int main() {
    int i;
    union POINT points[2];
    points[0].x = 2;
    points[0].y = 3;
    points[1].x = 4;
    points[1].y = 5;
    printf("\n point 1: x is %d y is %d", points[0].x, points[0].y);
    printf("\n point 2: x is %d y is %d", points[1].x, points[1].y);
}
