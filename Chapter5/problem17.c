#include <stdio.h>
#include <string.h>

typedef struct
{
    char name[20];
    char address[50];
    char grade[2];
    int num_rooms;
    int price;
}hotel_inf;

int main(){
    int i;

    hotel_inf hotel[10];
    strcpy(hotel[0].name, "Green Hotel");
    strcpy(hotel[0].address, "Green St.");
    strcpy(hotel[0].grade, "A");
    hotel[0].num_rooms = 100;
    hotel[0].price = 600;

    strcpy(hotel[1].name, "Blue Hotel");
    strcpy(hotel[1].address, "Blue St.");
    strcpy(hotel[1].grade, "A");
    hotel[1].num_rooms = 500;
    hotel[1].price = 900;

    strcpy(hotel[2].name, "Orange Hotel");
    strcpy(hotel[2].address, "Orange St.");
    strcpy(hotel[2].grade, "B");
    hotel[2].num_rooms = 1000;
    hotel[2].price = 200;

    printf("\n A GRADE HOTELS \n");
    printf("\n %s \n", hotel[0].grade);
    for (i = 0; i < 3; i++)
    {
        if (strcmp(hotel[i].grade, "A")  == 0)
        {
            printf("%s is a grade A hotel \n", hotel[i].name);
        }
        
    }

    printf("\n HOTELS LESS THAN 900$ \n");
    for (i = 0; i < 3; i++)
    {
        if (hotel[i].price < 900)
        {
            printf("%s is less than 900$ \n", hotel[i].name);
        }
        
    }
    
    
}