#include <stdio.h>
typedef struct POINT1
{
    int x, y;
};
typedef union POINT2
{
    int x;
    int y;
};

int main() {
    struct POINT1 P1 = {2, 3};
    // POINT2 P2 = {4, 5} DOES NOT WORK
    union POINT2 P2;
    printf("\n P1: %d and %d", P1.x, P1.y);
    P2.x = 4;
    printf("\n X coord P2: %d", P2.x);
    P2.y = 5;
    printf("\n Y coord P2: %d", P2.y);
    return 0;
}

