from cgitb import html
import requests
import urllib.request
from bs4 import BeautifulSoup

URL = "https://careercenter.apsva.us/staff-directory/"
r = requests.get(URL)
html_content = r.text
soup = BeautifulSoup(html_content, "html.parser")
print(soup.find_all('script'))