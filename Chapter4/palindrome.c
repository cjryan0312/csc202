#include <stdio.h>
#include <ctype.h>
#include <string.h>

void reverse(char *inpStr) {
    int i, len, temp;
    len = strlen(inpStr);

    for (i = 0; i < len/2; i++) {
        temp = inpStr[i];
        inpStr[i] = inpStr[len - i - 1];
        inpStr[len - i - 1] = temp;        
    }
}

int main() {
    int index = 0;
    char ch;
    char str[50];

    // file I/O, getting string from txt
    FILE *file;
    file = fopen("test1.txt", "r");
    if (file) {
        while ((ch = getc(file)) != EOF) {
            str[index] = ch;
            index++;
        }
        fclose(file);
    }

    str[0] = tolower(str[0]); // first char lowercase so result can be compared correctly
    char flipStr[50];
    int i = 0, strLength  = 0, result = 0, j = 0;

    // if character is not letter, then index is skipped 
    for (i = 0; str[i] != '\0'; ++i) {
        while ( !(str[i] >= 'a' && str[i] <= 'z') && !(str[i] >= 'A' && str[i] <= 'Z') && !(str[i] == '\0')) {
            for (j = i; str[j] != '\0'; ++j) {
                str[j] = tolower(str[j + 1]);
            }
            str[j] = '\0';
        }
    }
    
    strcpy(flipStr, str); //copy str to flip
    reverse(flipStr); // potential palindrome is reversed
    result = strcmp(str, flipStr); // if string is same forwards and backwards, it's a palindrome

    if (result == 0) {
        printf("\"%s\" is a panlindrome. \n", str);
    }
    else {
        printf("\"%s\" is not a panlindrome. ", str);
    }
}